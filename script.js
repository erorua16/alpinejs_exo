//Load js
$(document).ready(function() {

    //Dropdown menu for profile picture
    toggleMenu("#trigger",".dropdownProfileMenu")

});

//Onclick -- open menu
const toggleMenu = (trigger, menu) => {
    $(trigger).click(() => {
        //Toggle menu visibility
        $(menu).toggle();
    });
}